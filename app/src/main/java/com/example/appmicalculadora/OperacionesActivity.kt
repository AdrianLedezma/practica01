package com.example.appmicalculadora

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.appmycalculadora.Operaciones

class OperacionesActivity : AppCompatActivity() {
    private lateinit var txtUsuario: TextView
    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText
    private lateinit var txtResultado: TextView
    private lateinit var btnSumar: Button
    private lateinit var btnRestar: Button
    private lateinit var btnMult: Button
    private lateinit var btnDiv: Button
    private lateinit var btnCerrar: Button
    private lateinit var btnLimpiar: Button
    private lateinit var operaciones: Operaciones
    private var opcion: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operaciones)
        iniciarComponentes()
        eventosClic()
    }

    private fun iniciarComponentes() {
        txtUsuario = findViewById(R.id.txtUsuario)
        txtResultado = findViewById(R.id.txtResultado)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMult = findViewById(R.id.btnMult)
        btnDiv = findViewById(R.id.btnDiv)
        btnCerrar = findViewById(R.id.btnRegresar)
        btnLimpiar = findViewById(R.id.btnLimpiar)

        val bundle: Bundle? = intent.extras
        txtUsuario.text = bundle?.getString("name")
        operaciones = Operaciones(0f, 0f)
    }

    private fun validar(): Boolean {
        return txtNum1.text.toString().isNotEmpty() && txtNum2.text.toString().isNotEmpty()
    }

    private fun operacion(): Float {
        var res: Float = 0.0f
        try {
            if (validar()) {
                operaciones.num1 = txtNum1.text.toString().toFloat()
                operaciones.num2 = txtNum2.text.toString().toFloat()
                when (opcion) {
                    1 -> res = operaciones.suma()
                    2 -> res = operaciones.restar()
                    3 -> res = operaciones.multiplicar()
                    4 -> res = operaciones.dividir()
                }
            } else {
                throw IllegalArgumentException("Inserte valores válidos.")
            }
        } catch (e: IllegalArgumentException) {
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }
        return res
    }

    private fun eventosClic() {
        btnSumar.setOnClickListener {
            opcion = 1
            txtResultado.text= operacion().toString()
        }
        btnRestar.setOnClickListener {
            opcion = 2
            txtResultado.text= operacion().toString()
        }
        btnMult.setOnClickListener {
            opcion = 3
            txtResultado.text= operacion().toString()
        }
        btnDiv.setOnClickListener {
            if (txtNum2.text.toString().toFloat() == 0f) {
                txtResultado.text = "No es posible dividir sobre 0"
            } else {
                opcion = 4
                txtResultado.text= operacion().toString()
            }
        }

        btnLimpiar.setOnClickListener {
            txtResultado.text = ""
            txtNum2.text.clear()
            txtNum1.text.clear()
        }
        btnCerrar.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Calculadora")
            builder.setMessage("¿Desea cerrar?")
            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                finish()
            }
            builder.setNegativeButton(android.R.string.no, null)
            builder.show()
        }
    }
}
