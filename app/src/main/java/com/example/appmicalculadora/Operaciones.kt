package com.example.appmycalculadora

class Operaciones(var num1: Float, var num2: Float) {
    public fun suma() : Float {
        return this.num1 + this.num2;
    }
    public fun restar() : Float {
        return this.num1 - this.num2;
    }
    public fun multiplicar() : Float {
        return this.num1 * this.num2;
    }
    public fun dividir() : Float {
        if(this.num1!=0.0f && this.num2!=0.0f)
            return this.num1/this.num2
        else return 0.0f
    }
}